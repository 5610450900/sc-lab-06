package Abstract_methods_Prob;

public class Case {
	public String product(SmartPhone p){
		return p.product();
	}
	public String product(Apple a){
		return a.product();
	}
	public String topmodel(Sumsung s){
		return s.product();
	}
}
