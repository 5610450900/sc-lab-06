package Abstract_methods_Prob;

public abstract class SmartPhone {
	private String brand="None";
	public SmartPhone(String brand){
		this.brand = brand;
	}
	abstract String product();
}
